imdsTrain = imageDatastore('DataSetProg/PersoneAlex/training','IncludeSubfolders',true,'LabelSource','foldernames');
imdsValid = imageDatastore('DataSetProg/PersoneAlex/test','IncludeSubfolders',true,'LabelSource','foldernames');
imdsPred = imageDatastore('DataSetProg/PersoneAlex/prediction','IncludeSubfolders',true,'LabelSource','foldernames');
[imdsTrain] = splitEachLabel(imdsTrain,1);
[imdsValidation] = splitEachLabel(imdsValid,1);

net = alexnet;
net.Layers
inputSize = net.Layers(1).InputSize
layersTransfer = net.Layers(1:end-3);
numClasses = numel(categories(imdsTrain.Labels))
layers = [
    layersTransfer
    fullyConnectedLayer(numClasses,'WeightLearnRateFactor',20,'BiasLearnRateFactor',20)
    softmaxLayer
    classificationLayer];
pixelRange = [-30 30];
imageAugmenter = imageDataAugmenter( ...
    'RandXReflection',true, ...
    'RandXTranslation',pixelRange, ...
    'RandYTranslation',pixelRange);
augimdsTrain = augmentedImageDatastore(inputSize(1:2),imdsTrain, ...
    'DataAugmentation',imageAugmenter);
augimdsValidation = augmentedImageDatastore(inputSize(1:2),imdsValidation);
augimdsPrediction = augmentedImageDatastore(inputSize(1:2),imdsPred);
options = trainingOptions('sgdm', ...
    'MiniBatchSize',5, ...
    'MaxEpochs',2, ...
    'InitialLearnRate',1e-4, ...
    'ValidationData',augimdsValidation, ...
    'ValidationFrequency',3, ...
    'ValidationPatience',Inf, ...
    'Verbose',false, ...
    'Plots','training-progress');
netTransfer = trainNetwork(augimdsTrain,layers,options);
[YPred,scores] = classify(netTransfer,augimdsValidation);
%idx = randperm(numel(imdsValidation.Files),4);
%figure
%for i = 1:4
 %   subplot(2,2,i)
  % imshow(I)
   % label = YPred(idx(i));
    %title(string(label));
%end
YValidation = imdsValidation.Labels;
accuracy = mean(YPred == YValidation)
