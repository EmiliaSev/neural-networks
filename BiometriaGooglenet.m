
imdsTrain = imageDatastore('DataSet/training','IncludeSubfolders',true,'LabelSource','foldernames');
imdsValid = imageDatastore('DataSet/test','IncludeSubfolders',true,'LabelSource','foldernames');
imdsPred = imageDatastore('DataSet/prediction','IncludeSubfolders',true,'LabelSource','foldernames');
[imdsTrain] = splitEachLabel(imdsTrain,1);
[imdsValidation] = splitEachLabel(imdsValid,1);

net = googlenet;
lgraph = layerGraph(net);
figure('Units','normalized','Position',[0.1 0.1 0.8 0.8]);
plot(lgraph)
net.Layers(1)
inputSize = net.Layers(1).InputSize;
lgraph = removeLayers(lgraph, {'loss3-classifier','prob','output'});

numClasses = numel(categories(imdsTrain.Labels));
newLayers = [
    fullyConnectedLayer(numClasses,'Name','fc','WeightLearnRateFactor',10,'BiasLearnRateFactor',10)
    softmaxLayer('Name','softmax')
    classificationLayer('Name','classoutput')];
lgraph = addLayers(lgraph,newLayers);
lgraph = connectLayers(lgraph,'pool5-drop_7x7_s1','fc');

figure('Units','normalized','Position',[0.3 0.3 0.4 0.4]);
plot(lgraph)
ylim([0,10])
pixelRange = [-30 30];
imageAugmenter = imageDataAugmenter( ...
    'RandXReflection',true, ...
    'RandXTranslation',pixelRange, ...
    'RandYTranslation',pixelRange);
augimdsTrain = augmentedImageDatastore(inputSize(1:2),imdsTrain, ...
    'DataAugmentation',imageAugmenter);
augimdsValidation = augmentedImageDatastore(inputSize(1:2),imdsValidation);
augimdsPrediction = augmentedImageDatastore(inputSize(1:2),imdsPred);
options = trainingOptions('sgdm', ...
    'MiniBatchSize',10, ...
    'MaxEpochs',6, ...
    'InitialLearnRate',1e-4, ...
    'ValidationData',augimdsValidation, ...
    'ValidationFrequency',3, ...
    'ValidationPatience',Inf, ...
    'Verbose',false ,...
    'Plots','training-progress');
net = trainNetwork(augimdsTrain,lgraph,options);
[YPred,probs] = classify(net,augimdsPrediction);
accuracy = mean(YPred == imdsPred.Labels)
%idx = randperm(numel(imdsValidation.Files),4);
%figure
%for i = 1:4
%    subplot(2,2,i)
%    I = readimage(imdsValidation,idx(i));
%    imshow(I)
%    label = YPred(idx(i));
%    title(string(label) + ", " + num2str(100*max(probs(idx(i),:)),3) + "%");
%end